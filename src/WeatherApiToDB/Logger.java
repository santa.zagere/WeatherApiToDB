package WeatherApiToDB;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;


public class Logger {
    public static void Append(String logMessage)
    {
        Date date = new Date();
        // Define wheres the log file
        File file = new File(Main.LogPath);
        BufferedWriter writer = null;
        //Using Bufferwriter append string to log, and insert timestamp before each line.
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            writer.append(date+": "+logMessage+"\n");
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

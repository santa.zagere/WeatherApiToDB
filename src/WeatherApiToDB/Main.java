package WeatherApiToDB;
import com.google.gson.*;
import java.util.Scanner;
import java.io.IOException;


public class Main {
    //Logfile location, defined here, so it can be used in Logger class
    public static String LogPath = "C:/temp/WeatherApiToDB.log";

    public static void main(String[] args) throws IOException, InterruptedException {
        // MySql database configuration
        final String DbUrl = "jdbc:mysql://localhost:3306/java27";
        final String User = "root";
        final String Password = "Latvija2022";
        // Api configuration, for connection
        // https://rapidapi.com/community/api/open-weather-map
        final String ApiHostHeaderName = "X-RapidAPI-Host";
        final String ApiHostHeaderContent = "community-open-weather-map.p.rapidapi.com";
        final String ApiKeyHeaderName = "X-RapidAPI-Key";
        final String ApiKeyHeaderContents = "322e8801e0msh3fae249aa7363d2p129017jsnc05a6588613e";


        // declare some strings, that will be used later
        String Country;
        String City = null;
        Boolean Success;

        // create char with value y, so it can be used in while loop
        char again = 'y';
        //declare scanner, to read user input
        Scanner scanner = new Scanner(System.in);

        //if y is pressed, proceed with loop
        while (again=='y') {
            Success = true;
            Country = null;
            System.out.println("Please choose country, enter a number 1-3!");
            System.out.println("1 - Latvia");
            System.out.println("2 - Lithuania");
            System.out.println("3 - Estonia");
            char countryaction = scanner.nextLine().charAt(0);

            if(countryaction == '1'){
                Country = "Latvia";
                System.out.println("Chosen Country "+Country);

            }else if (countryaction == '2'){
                Country = "Lithuania";
                System.out.println("Chosen Country "+Country);;

            }else if (countryaction == '3'){
                Country = "Estonia";
                System.out.println("Chosen Country "+Country);

            } else {
                System.out.println("invalid input, please try again");
                System.out.println( "Enter 'y' if you want to try again" );
                Success = false;
            }
            if (Success) {
                System.out.println("Please choose city, enter a number 1-3!");
                if (Country == "Latvia") {
                    System.out.println("1 - Riga");
                    System.out.println("2 - Daugavpils");
                    System.out.println("3 - Ventspils");
                } else if (Country == "Lithuania") {
                    System.out.println("1 - Vilnius");
                    System.out.println("2 - Klaipeda");
                    System.out.println("3 - Palanga");
                } else if (Country == "Estonia") {
                    System.out.println("1 - Tallin");
                    System.out.println("2 - Tartu");
                    System.out.println("3 - Narva");
                }

                // Get city number choice from user
                char cityaction = scanner.nextLine().charAt(0);

                if (Country == "Latvia") {
                    if (cityaction == '1') {
                        City = "Riga";
                    } else if (cityaction == '2') {
                        City = "Daugavpils";
                    } else if (cityaction == '3') {
                        City = "Ventspils";
                    } else {
                        Success = false;
                    }
                } else if (!Success) {
                    System.out.println("Invalid input, please try again");
                    System.out.println("Enter 'y' if you want to try again to choose the city");
                } else if (Country == "Lithuania") {
                    if (cityaction == '1') {
                        City = "Vilnius";
                    } else if (cityaction == '2') {
                        City = "Klaipeda";
                    } else if (cityaction == '3') {
                        City = "Palanga";
                    } else {
                        System.out.println("Invalid input, please try again");
                        System.out.println("Enter 'y' if you want to try again to choose the city");
                        Success = false;
                    }
                } else if (Country == "Estonia") {
                    if (cityaction == '1') {
                        City = "Tallin";
                    } else if (cityaction == '2') {
                        City = "Tartu";
                    } else if (cityaction == '3') {
                        City = "Narva";
                    } else {
                        System.out.println("Invalid input, please try again");
                        System.out.println("Enter 'y' if you want to try again to choose the city");
                        Success = false;
                    }
                }
            }



            if (Success) {
                // Merge city and country strings into api url
                String Url = "https://community-open-weather-map.p.rapidapi.com/weather?q=" + City + "%2C" + Country + "&lat=0&lon=0&lang=en&units=metric";
                Logger.Append("Fetching data from API " + Url);
                //Call Api using GetFromApi class with method ApiConnect, and store as JsonFile String
                String JsonFile = GetFromApi.ApiConnect(Url, ApiHostHeaderName, ApiHostHeaderContent, ApiKeyHeaderName, ApiKeyHeaderContents);
                //Continue If content of string JsonFile is not error
                if (JsonFile != "error") {
                    //Parse JSON string in to Java object using gjson library
                    JsonElement jsonElement = new JsonParser().parse(JsonFile);
                    //extract temperature data from object
                    Double Temp = (jsonElement.getAsJsonObject().get("main").getAsJsonObject().get("temp")).getAsDouble();
                    //extract location id from object
                    Integer Id = (jsonElement.getAsJsonObject().get("id")).getAsInt();
                    // Print info message
                    System.out.println("The current temperature in " + Country + "," + City + " is " + Temp + " celsius");

                    // INSERT DATA TO MYSQL DATABASE using DbActions class, InsertWeatherData methon.
                    DbActions.InsertWeatherData(DbUrl, User, Password, Temp, Country, City, Id);

                    Logger.Append("Received Json file: " + JsonFile);
                } else {
                    System.out.println("Could not fetch data from API, please check logfile at" + LogPath);
                }
            }
            //Restart while loop if needed
            System.out.println("press y if you want to try again");
            again = scanner.nextLine().charAt(0);
        }
    }
}

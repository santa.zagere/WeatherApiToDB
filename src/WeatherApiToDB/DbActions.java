package WeatherApiToDB;
import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbActions {


    public static void InsertWeatherData(String DbUrl, String User, String Password, Double Temp, String Country, String City, Integer Id) {
        // Connect to DB
        try (Connection conn = DriverManager.getConnection(DbUrl, User, Password)) {
            System.out.println("Connected to db");

            // Create table, if doesnt exist.
            final String CreateTableIfNotExist = "CREATE TABLE IF NOT EXISTS WeatherApiToDB (`date` DATETIME DEFAULT CURRENT_TIMESTAMP, `temp` DOUBLE, `id` INT, `country` VARCHAR(50) CHARACTER SET utf8, `city` VARCHAR(50) CHARACTER SET utf8);";
            try {
                int tableCreated = conn.createStatement().executeUpdate(CreateTableIfNotExist);

            } catch(SQLException e) {
                System.out.println("Something went wrong" + e);
            }

            //Create string to Insert data
            final String sql = "INSERT INTO WeatherApiToDB (temp, city, country, id) VALUES (?, ?, ?, ?);";
            // Prepare SQL statement, using string
            PreparedStatement InsertWeatherData = conn.prepareStatement(sql);
            InsertWeatherData.setDouble(1, Temp);
            InsertWeatherData.setString(2, City);
            InsertWeatherData.setString(3, Country);
            InsertWeatherData.setInt(4, Id);
            // Run SQL statement
            int rowInserted = InsertWeatherData.executeUpdate();
            if (rowInserted > 0) {
                System.out.println("Entry added successfully");
            } else {
                System.out.println("Something went wrong");
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong" + e);
        }

    }
}

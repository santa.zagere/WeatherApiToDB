# WeatherApiToDB
This program calls external API service and collects temperature data for selected region and stores it on MYSQL database.
In this particular case this api provider is being used:
https://openweathermap.org/api

## Requirements
This program requires MYSQL to store data, tested only on windows 11 and mysql 8.0

## Installation

To start using the program, please change the variables in the begginig of the Main class, and if some other weather API is being used, you will have to edit the API url in main method, so it would be possible to change different locations.

The program creates mysql table automatically if it doesnt exist.

![Diagramm](https://gitlab.com/santa.zagere/WeatherApiToDB/-/raw/a61eca52f32be68e747fcd0e0c191be4c640889e/WeatherApiToMysql.png "Diagram")
